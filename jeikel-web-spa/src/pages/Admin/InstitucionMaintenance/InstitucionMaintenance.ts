import { GenericPage, Component, Vue } from '../baseExporter'
import { InstitucionModel } from '../../../Models/InstitucionModel'
import axios from 'axios'

@Component({
  template: require('./InstitucionMaintenance.html'),
  components: {  }
})
export class InstitucionMaintenance extends GenericPage<InstitucionModel> {

  apiUrl = "http://localhost:8080/api/Institutions/"; 
  instituciones : Array<InstitucionModel> = [];
  constructor() {
    super('', {
      nombre: {
        label: 'Nombre',
        sortable: true
      },
      descripcion: {
        label: 'Descripcion',
        sortable: false
      },
    }, InstitucionModel)
  }
  initModel(){
    this.model.name = null;
    this.model.description = null;
    this.model.id = null;
  }

  mounted() {
      this.getData();
  }

    getData(){
      axios.get(this.apiUrl).then((response)=>{
        this.instituciones = response.data
        }).catch(()=>{
          alert("Error")
        })
    }

    saveData(){
      var dataToSave =  JSON.stringify(this.model)
        if(this.model.id){
          axios.put(this.apiUrl,this.model).then(()=>{

            alert("Editado Correctamente")
            this.getData();
            this.initModel();
            
          }).catch(()=>{
            alert("Error")
          })

        }else{

          axios.post(this.apiUrl,this.model).then((response)=>{
            alert('Guardaddo Correctamente');
            this.getData();
            this.initModel();
          }).catch(()=>{
            alert("Error")
          })
        }

    }

    mapData(institucion: InstitucionModel){
      this.model =  JSON.parse(JSON.stringify(institucion)) as any;

    }

    deleteData(institucion){
      
      var result = confirm("Esta seguro que desea borrar " + institucion.name )
      if(result) {
        axios.delete(this.apiUrl + institucion.id).then(()=>{
            alert(" Borrado Correctamente")
              this.getData();
              this.initModel();
        }).catch(()=>{
          alert("Error")
        })

      }
        

    }
}
