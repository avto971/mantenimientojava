import { BaseModel } from './Generic/BaseModel'
export class InstitucionModel extends BaseModel {
  id:number;
  name:string;
  description:number;
  constructor() {

    super()
    this.id = null;
    this.name = null;
    this.description = null;

  }

}
