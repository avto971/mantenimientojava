package com.avto.Services;

import com.avto.Dao.InstitutionsDao;
import com.avto.Entities.Institutions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class InstitutionsService {

    @Autowired
    private InstitutionsDao InstitutionsDao;

    public List<Institutions> getAllInstitutions(){
        return this.InstitutionsDao.getInstitutions();
    }

    public boolean createInstitution(Institutions Institution){
        return this.InstitutionsDao.createInstitution(Institution);
    }
    public boolean deleteInstitution(int id){
        return this.InstitutionsDao.deleteInstitution(id);
    }

    public boolean updateInstitution(Institutions institutions){
        return this.InstitutionsDao.updateInstitution(institutions);
    }

    public Institutions getInstitutionById(int id){
        return this.InstitutionsDao.getInstitutionById(id);
    }
}
