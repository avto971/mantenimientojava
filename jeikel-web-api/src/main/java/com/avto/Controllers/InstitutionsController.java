package com.avto.Controllers;

import com.avto.Entities.Institutions;
import com.avto.Services.InstitutionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;
import java.util.List;

@RestController
@RequestMapping("api/Institutions")
 public class InstitutionsController {

    @Autowired
    private InstitutionsService InstitutionsService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Institutions> getAllInstitution(){
        return InstitutionsService.getAllInstitutions();
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public boolean createInstitution(@RequestBody Institutions Institution){
        return InstitutionsService.createInstitution(Institution);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Institutions getInstitutionById(@PathVariable("id") int id){
        return InstitutionsService.getInstitutionById(id);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public boolean updateInstitution(@RequestBody Institutions Institution){
        return InstitutionsService.updateInstitution(Institution);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public boolean deleteInstitution(@PathVariable("id") int id){
        return InstitutionsService.deleteInstitution(id);
    }
}
