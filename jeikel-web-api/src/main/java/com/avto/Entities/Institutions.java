package com.avto.Entities;

import java.util.Date;

public class Institutions {

    private int Id;
    private String Name;
    private String Description;
    private Boolean Deleted;
    private Date CreatedDate;
    private Date ModifiedDate;

    public Institutions() {
    }

    public Institutions(int id, String name, String description, Boolean deleted, Date createdDate, Date modifiedDate) {
        Id = id;
        Name = name;
        Description = description;
        Deleted = deleted;
        CreatedDate = createdDate;
        ModifiedDate = modifiedDate;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Boolean getDeleted() {
        return Deleted;
    }

    public void setDeleted(Boolean deleted) {
        Deleted = deleted;
    }

    public Date getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(Date createdDate) {
        CreatedDate = createdDate;
    }

    public Date getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        ModifiedDate = modifiedDate;
    }
}

