package com.avto.Dao;

import com.avto.Entities.Institutions;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
public class InstitutionsDao {


    public List<Institutions> getInstitutions(){
        Connection con = getConnection();
        List<Institutions> Institutions = new ArrayList<Institutions>() {
        };
        if(con == null){
            return null;
        }else {
            Statement state;
            String query = "Select * from Institutions where Deleted = 0";
            try{
                state = con.createStatement();
                ResultSet rs = state.executeQuery(query);

                while(rs.next()){
                    Institutions newInstitutions = new Institutions(
                            rs.getInt("Id"),
                            rs.getString("Name"),
                            rs.getString("Description"),
                            rs.getBoolean("Deleted"),
                            rs.getDate("CreatedDate"),
                            rs.getDate("ModifiedDate")
                    );
                    newInstitutions.setId(rs.getInt("Id"));
                    Institutions.add(newInstitutions);
                }
            }catch (SQLException ex){
                return null;
            } catch (Exception e){
                return null;
            }
            return Institutions;
        }
    }

    public boolean createInstitution(Institutions Institution) {
        Institutions InstitutionToCheck;
        InstitutionToCheck = getInstitutionByName(Institution.getName());
        if(InstitutionToCheck != null){
            return false;
        }else{
            String query = "INSERT INTO Institutions VALUES ('"+Institution.getName()+"','"+
                    Institution.getDescription()+"',"+
                    0+",'"+
                    LocalDateTime.now()+"','"+
                    LocalDateTime.now()+"')";
            return executeUpdate(query);

        }
    }

    public boolean deleteInstitution(int id){
        String query = "UPDATE Institutions SET Deleted = 1 WHERE id ="+ id;
        return executeUpdate(query);
    }

    public Institutions getInstitutionById(int id){
        String query = "Select * from Institutions WHERE id ="+ id;
        return getInstitution(query);
    }

    private Institutions getInstitutionByName(String name){
        String query = "Select * from Institutions WHERE Name ="+ name;
        return getInstitution(query);
    }

    private Institutions getInstitution(String query){
        try {
            Institutions Institutions = new Institutions();
            Connection con = getConnection();
            Statement state;
            state = con.createStatement();
            ResultSet rs = state.executeQuery(query);

            while (rs.next()) {
                Institutions newInstitutions = new Institutions(
                        rs.getInt("Id"),
                        rs.getString("Name"),
                        rs.getString("Description"),
                        rs.getBoolean("Deleted"),
                        rs.getDate("CreatedDate"),
                        rs.getDate("ModifiedDate")
                );
                newInstitutions.setId(rs.getInt("Id"));
                Institutions = newInstitutions;
            }
            return Institutions;
        } catch (SQLException ex) {
            return null;
        } catch (Exception e) {
            return null;
        }
    }
    private boolean executeUpdate(String query){
        try {
            Connection con = getConnection();
            Statement state;
            state = con.createStatement();
            int result = state.executeUpdate(query);

            if(result ==1){
                return true;
            }
            return false;
        } catch (SQLException ex) {
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    private Connection getConnection() {
        try{
            String connectionUrl = "jdbc:sqlserver://localhost\\.\\sqlexpress:64504;database=Reto2;user=admin;password=Code1234,";
            Connection cnn = DriverManager.getConnection(connectionUrl);

            return cnn;
        }catch (SQLException ex){
            return null;
        }

    }

    public boolean updateInstitution(Institutions Institutions){

            String query = "UPDATE Institutions SET Name = '"+Institutions.getName()+
                    "', Description = '"+ Institutions.getDescription()+
                    "', ModifiedDate ='"+ LocalDateTime.now() +"' WHERE id ="+ Institutions.getId();
            return executeUpdate(query);


    }


}
